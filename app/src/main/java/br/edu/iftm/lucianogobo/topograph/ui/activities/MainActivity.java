package br.edu.iftm.lucianogobo.topograph.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.edu.iftm.lucianogobo.topograph.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickNewInputData(View view){
        Intent newInputData = new Intent(this,NewInputData.class);
        startActivity(newInputData);
    }
}