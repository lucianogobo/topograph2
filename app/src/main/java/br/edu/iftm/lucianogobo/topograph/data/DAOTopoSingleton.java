package br.edu.iftm.lucianogobo.topograph.data;

import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph.model.Topo;

public class DAOTopoSingleton {

    private static DAOTopoSingleton INSTANCE;

    //-----recurso globalmente acessado
    private ArrayList<Topo> topos;


    private DAOTopoSingleton(){
        this.topos =new ArrayList<>();
    }

    //Garantir que INSTANCE é modificado apenas 1 vez
    public static DAOTopoSingleton getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE =new DAOTopoSingleton();
        }
        return INSTANCE;
    }
    //Definir regras aos recursos globais

    public ArrayList<Topo> getTopos(){
        return this.topos;
    }

    public void addStore(Topo topo){
        this.topos.add(topo);
    }

    public void modStore(int i,Topo topo){
        this.topos.add(i,topo);
    }

    public void removeStore(Topo topo) {this.topos.remove(topo);}

}
