package br.edu.iftm.lucianogobo.topograph.ui.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import br.edu.iftm.lucianogobo.topograph.R;
import br.edu.iftm.lucianogobo.topograph.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph.model.Topo;
import br.edu.iftm.lucianogobo.topograph.ui.activities.NewInputData.numberOfVertices;

public class NewTopogActivity extends AppCompatActivity {

    public static final String RESULT_KEY="NewTopogrActivity.RESULT_KEY";
    private TextView editPoint;
    private TextView editDirection;
    private TextView editAngularRe;
    private TextView editAngularVante;
    private TextView editZenit;
    private TextView editLineMinus;
    private TextView editLineMedium;
    private TextView editLineHight;
    private TextView editAltTeodolite;
    private static int contador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_topog);
        this.editPoint = findViewById(R.id.editPoint);
        this.editDirection = findViewById(R.id.editDirection);
        this.editAngularRe = findViewById(R.id.editAngularRe);
        this.editAngularVante = findViewById(R.id.editAngularVante);
        this.editZenit = findViewById(R.id.editZenit);
        this.editLineMinus = findViewById(R.id.editLineMinus);
        this.editLineMedium = findViewById(R.id.editLineMedium);
        this.editLineHight = findViewById(R.id.editLineHight);
        this.editAltTeodolite = findViewById(R.id.editAltTeodolite);
        contador=0;
    }

    public void onSave(View view) {
        int NumeroVertices = numberOfVertices;
        contador = contador + 1;
            String point = this.editPoint.getText().toString();
            int ponto = Integer.parseInt(point);
            String direction = this.editDirection.getText().toString();
            String angularRe = this.editAngularRe.getText().toString();
            String angularVante = this.editAngularVante.getText().toString();
            String zenit = this.editZenit.getText().toString();
            String lineMinus = this.editLineMinus.getText().toString();
            String lineMedium = this.editLineMedium.getText().toString();
            String lineHight = this.editLineHight.getText().toString();
            String altTeodolite = this.editAltTeodolite.getText().toString();
            double alturaTeodolito = Double.parseDouble(altTeodolite);
            String anguloHorizontal = "nao_disponivel";
            double distancia = 0.00;
            String anguloCorrigido = "nao_disponivel";
            String azimute = "nao_disponivel";
            double distanciaX = 0.00;
            double distanciaY = 0.00;
            double distanciaZ = 0.00;
            double cota = 0.00;
            double deltaH = 0.00;

            if (point.isEmpty() || direction.isEmpty() || angularRe.isEmpty() ||
                    angularVante.isEmpty() || zenit.isEmpty() || lineMinus.isEmpty() ||
                    lineMedium.isEmpty() || lineHight.isEmpty() || altTeodolite.isEmpty()) {
                return;
            }

            Topo topo = new Topo(ponto, direction, angularRe, angularVante, zenit, lineMinus, lineMedium,
                    lineHight, alturaTeodolito, anguloHorizontal, distancia, anguloCorrigido,
                    azimute, distanciaX, distanciaY, distanciaZ, cota, deltaH);

            DAOTopoSingleton.getINSTANCE().addStore(topo);
            editPoint.setText("");
            editDirection.setText("");
            editAngularRe.setText("");
            editAngularVante.setText("");
            editZenit.setText("");
            editLineMinus.setText("");
            editLineMedium.setText("");
            editLineHight.setText("");
            editAltTeodolite.setText("");
        if(contador == NumeroVertices){
            Intent newTableData = new Intent(this,TopoTableOne.class);
            startActivity(newTableData);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}