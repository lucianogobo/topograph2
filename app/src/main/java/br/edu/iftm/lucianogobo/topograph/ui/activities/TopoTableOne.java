package br.edu.iftm.lucianogobo.topograph.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import br.edu.iftm.lucianogobo.topograph.R;
import br.edu.iftm.lucianogobo.topograph.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph.model.Topo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

public class TopoTableOne extends AppCompatActivity {
    public static final String RESULT_KEY="NewTopoTableActivity.RESULT_KEY";
    static int modifyVertice;
    private TextView cell21;
    private TextView cell22;
    private TextView cell23;
    private TextView cell24;
    private TextView cell25;
    private TextView cell26;
    private TextView cell27;
    private TextView cell28;
    private TextView cell29;
    private TextView cell211;
    private TextView cell31;
    private TextView cell32;
    private TextView cell33;
    private TextView cell34;
    private TextView cell35;
    private TextView cell36;
    private TextView cell37;
    private TextView cell38;
    private TextView cell39;
    private TextView cell311;
    private TextView cell41;
    private TextView cell42;
    private TextView cell43;
    private TextView cell44;
    private TextView cell45;
    private TextView cell46;
    private TextView cell47;
    private TextView cell48;
    private TextView cell49;
    private TextView cell411;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topo_table_one);
        this.cell21=findViewById(R.id.cell21);
        this.cell22=findViewById(R.id.cell22);
        this.cell23=findViewById(R.id.cell23);
        this.cell24=findViewById(R.id.cell24);
        this.cell25=findViewById(R.id.cell25);
        this.cell26=findViewById(R.id.cell26);
        this.cell27=findViewById(R.id.cell27);
        this.cell28=findViewById(R.id.cell28);
        this.cell29=findViewById(R.id.cell29);
        this.cell211=findViewById(R.id.cell211);
        this.cell31=findViewById(R.id.cell31);
        this.cell32=findViewById(R.id.cell32);
        this.cell33=findViewById(R.id.cell33);
        this.cell34=findViewById(R.id.cell34);
        this.cell35=findViewById(R.id.cell35);
        this.cell36=findViewById(R.id.cell36);
        this.cell37=findViewById(R.id.cell37);
        this.cell38=findViewById(R.id.cell38);
        this.cell39=findViewById(R.id.cell39);
        this.cell311=findViewById(R.id.cell311);
        this.cell41=findViewById(R.id.cell41);
        this.cell42=findViewById(R.id.cell42);
        this.cell43=findViewById(R.id.cell43);
        this.cell44=findViewById(R.id.cell44);
        this.cell45=findViewById(R.id.cell45);
        this.cell46=findViewById(R.id.cell46);
        this.cell47=findViewById(R.id.cell47);
        this.cell48=findViewById(R.id.cell48);
        this.cell49=findViewById(R.id.cell49);
        this.cell411=findViewById(R.id.cell411);
        this.buildStoreList();
    }

    private void clearStoreList(){
        cell21.setText("");
        cell22.setText("");
        cell23.setText("");
        cell24.setText("");
        cell25.setText("");
        cell26.setText("");
        cell27.setText("");
        cell28.setText("");
        cell29.setText("");
        cell31.setText("");
        cell32.setText("");
        cell33.setText("");
        cell34.setText("");
        cell35.setText("");
        cell36.setText("");
        cell37.setText("");
        cell38.setText("");
        cell39.setText("");
        cell41.setText("");
        cell42.setText("");
        cell43.setText("");
        cell44.setText("");
        cell45.setText("");
        cell46.setText("");
        cell47.setText("");
        cell48.setText("");
        cell49.setText("");
    }

    private void buildStoreList() {
        int contador=0;
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
             contador=contador+1;
             if(contador==1){
                 cell21.setText(Integer.toString(topo.getPonto()));
                 cell22.setText(topo.getDirecao());
                 cell23.setText(topo.getAnguloRE());
                 cell24.setText(topo.getAnguloVANTE());
                 cell25.setText(topo.getZenite());
                 cell26.setText(topo.getFioInferior());
                 cell27.setText(topo.getFioMedio());
                 cell28.setText(topo.getFioSuperior());
                 cell29.setText(Double.toString(topo.getAlturaAparelho()));
             }
            if(contador==2){
                cell31.setText(Integer.toString(topo.getPonto()));
                cell32.setText(topo.getDirecao());
                cell33.setText(topo.getAnguloRE());
                cell34.setText(topo.getAnguloVANTE());
                cell35.setText(topo.getZenite());
                cell36.setText(topo.getFioInferior());
                cell37.setText(topo.getFioMedio());
                cell38.setText(topo.getFioSuperior());
                cell39.setText(Double.toString(topo.getAlturaAparelho()));
            }
            if(contador==3){
                cell41.setText(Integer.toString(topo.getPonto()));
                cell42.setText(topo.getDirecao());
                cell43.setText(topo.getAnguloRE());
                cell44.setText(topo.getAnguloVANTE());
                cell45.setText(topo.getZenite());
                cell46.setText(topo.getFioInferior());
                cell47.setText(topo.getFioMedio());
                cell48.setText(topo.getFioSuperior());
                cell49.setText(Double.toString(topo.getAlturaAparelho()));
            }
        }
    }

    public void onClick(View view){
        int ponto;
        int idtrash;
        TextView ttexto;
        String texto;
        idtrash=view.getId();
        ttexto=findViewById(idtrash);
        texto=ttexto.getText().toString();
        ponto=Integer.parseInt(texto);
        deleteStoreList(ponto);
    }

    public void onClick2(View view){
        int ponto;
        int idtrash;
        TextView ttexto;
        String texto;
        idtrash=view.getId();
        ttexto=findViewById(idtrash);
        texto=ttexto.getText().toString();
        ponto=Integer.parseInt(texto);
        modifyStoreList(ponto);
    }

    private void modifyStoreList(int ponto){
        for (int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++) {
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto()==ponto){
                modifyVertice=ponto;
                Intent newModifyData = new Intent(this, TopoModify.class);
                startActivity(newModifyData);
                finish();
            }
        }
    }

    private void deleteStoreList(int ponto){
        for (int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++) {
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto()==ponto){
                DAOTopoSingleton.getINSTANCE().removeStore(DAOTopoSingleton.getINSTANCE().getTopos().get(i));
                if(i==0){
                    cell211.setText(Integer.toString(i+2));
                    cell311.setText(Integer.toString(i+3));
                }
                if(i==1){
                    cell311.setText(Integer.toString(i+2));
                }
            }
        }
        clearStoreList();
        buildStoreList();
    }

}