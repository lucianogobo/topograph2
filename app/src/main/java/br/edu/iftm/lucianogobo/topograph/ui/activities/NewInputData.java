package br.edu.iftm.lucianogobo.topograph.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.lucianogobo.topograph.R;

public class NewInputData extends AppCompatActivity {

    public static final String RESULT_KEY="NewInitialTopoActivity.RESULT_KEY";
    static int numberOfVertices;
    static String azimuthFirst;
    private TextView editNumberOfVertices;
    private TextView editAzimuthFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_input_data);
        this.editNumberOfVertices = findViewById(R.id.editNumberOfVertices);
        this.editAzimuthFirst = findViewById(R.id.editAzimuthFirst);
    }

    public void onInsert(View view){
        String numeroVertices = this.editNumberOfVertices.getText().toString();
        numberOfVertices= Integer.parseInt(numeroVertices);
        String azimuteInicial = this.editAzimuthFirst.getText().toString();
        azimuthFirst=azimuteInicial;

        if(numeroVertices.isEmpty() || azimuteInicial.isEmpty()){
            return;
        }else{
            Intent openNewStoreActivity = new Intent(this, NewTopogActivity.class);
            startActivity(openNewStoreActivity);
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

}