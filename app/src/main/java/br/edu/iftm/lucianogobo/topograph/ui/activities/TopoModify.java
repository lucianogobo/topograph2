package br.edu.iftm.lucianogobo.topograph.ui.activities;

import static br.edu.iftm.lucianogobo.topograph.ui.activities.TopoTableOne.modifyVertice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.lucianogobo.topograph.R;
import br.edu.iftm.lucianogobo.topograph.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph.model.Topo;

public class TopoModify extends AppCompatActivity {

    public static final String RESULT_KEY="NewTopoTableActivity.RESULT_KEY";
    private TextView Mod1;
    private TextView Mod2;
    private TextView Mod3;
    private TextView Mod4;
    private TextView Mod5;
    private TextView Mod6;
    private TextView Mod7;
    private TextView Mod8;
    private TextView Mod9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topo_modify);
        this.Mod1 = findViewById(R.id.Mod1);
        this.Mod2 = findViewById(R.id.Mod2);
        this.Mod3 = findViewById(R.id.Mod3);
        this.Mod4 = findViewById(R.id.Mod4);
        this.Mod5 = findViewById(R.id.Mod5);
        this.Mod6 = findViewById(R.id.Mod6);
        this.Mod7 = findViewById(R.id.Mod7);
        this.Mod8 = findViewById(R.id.Mod8);
        this.Mod9 = findViewById(R.id.Mod9);
        buildStoreList();
    }

    private void buildStoreList() {
          int verticeModificado=modifyVertice;
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
            if(topo.getPonto()==verticeModificado){
                Mod1.setText(Integer.toString(topo.getPonto()));
                Mod2.setText(topo.getDirecao());
                Mod3.setText(topo.getAnguloRE());
                Mod4.setText(topo.getAnguloVANTE());
                Mod5.setText(topo.getZenite());
                Mod6.setText(topo.getFioInferior());
                Mod7.setText(topo.getFioMedio());
                Mod8.setText(topo.getFioSuperior());
                Mod9.setText(Double.toString(topo.getAlturaAparelho()));
            }
        }
    }

    public void modifyText(View view){
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
            if(topo.getPonto()==modifyVertice){
                topo.setPonto(Integer.parseInt(Mod1.getText().toString()));
                topo.setDirecao(Mod2.getText().toString());
                topo.setAnguloRE(Mod3.getText().toString());
                topo.setAnguloVANTE(Mod4.getText().toString());
                topo.setZenite(Mod5.getText().toString());
                topo.setFioInferior(Mod6.getText().toString());
                topo.setFioMedio(Mod7.getText().toString());
                topo.setFioSuperior(Mod8.getText().toString());
                topo.setAlturaAparelho(Double.parseDouble(Mod9.getText().toString()));
            }
        }
        Intent newTableData = new Intent(this,TopoTableOne.class);
        startActivity(newTableData);
        finish();
    }

}